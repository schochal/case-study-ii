clear all;
clc;
close all; 

%%%CONSTANTS
%___________________________________________
R=8.314; %KJ/molK
V=15.36; %m3
A=10; %CONTROLLARE_________________


f_tot=4.5;
init=[1,3,0.5];
f=zeros(3,1);
intV=[0:0.01:15.36];


A_n2= 19.50583; %500-2000 K
B_n2= 19.88705;
C_n2= -8.598535;
D_n2= 1.369784;
E_n2= 0.527601;
F_n2= -4.935201;
H_n2= 0;
A_h2= 33.066178; %298-1000 K
B_h2= -11.363417;
C_h2= 11.432816;
D_h2= -2.772874;
E_h2= -0.158558;
F_h2= -9.980797;
H_h2= 0;
A_nh3= 19.99563;
B_nh3= 49.77119;
C_nh3= -15.37599;
D_nh3= 1.921168;
E_nh3= 0.189174;
F_nh3= -53.30667;
H_nh3= -45.89806;

H0_n2=0;
H0_h2=0;
H0_nh3=-46; %Kj/mol

t=773; %K %CONTROLLARE_________________
p=200; %atm %CONTROLLARE_________________ calculate partial p

U=25; %W/m2*K
Ta=283; %K %CONTROLLARE_________________
a=A/V; %CONTROLLARE_________________
%___________________________________________

Cp_n2=A_n2+B_n2*t+C_n2*t^2+D_n2*t^3+E_n2/t^2;
Cp_h2=A_h2+B_h2*t+C_h2*t^2+D_h2*t^3+E_h2/t^2;
Cp_nh3=A_nh3+B_nh3*t+C_nh3*t^2+D_nh3*t^3+E_nh3/t^2;

H_n2=H0_n2+A_n2*t+B_n2*t^2/2+C_n2*t^3/3+D_n2*t^4/4-E_n2/t+F_n2-H_n2;
H_h2=H0_h2+A_h2*t+B_h2*t^2/2+C_h2*t^3/3+D_h2*t^4/4-E_h2/t+F_h2-H_h2;
H_nh3=H0_nh3+A_nh3*t+B_nh3*t^2/2+C_nh3*t^3/3+D_nh3*t^4/4-E_nh3/t+F_nh3-H_nh3;

deltaH=2*H_nh3-H_n2-3*H_h2;
%___________________________________________



k1=1.0e4*exp((-9.1e4)/(R*t));
k2=1.3e10*exp((-1.41e5)/(R*t));


%___________________________________________
%%% MASS BALANCE PFR
%1=n2
%2=h2
%3=nh3
%
%for i=1:n_step
%    
%    p_n2(i)=p*f_n2(i)/f_tot;
%    p_h2(i)=p*f_h2(i)/f_tot;
%    p_nh3(i)=p*f_nh3(i)/f_tot;
%    
%    r_n2(i)=-k1.*p_n2(i).^0.5.*p_h2(i).^1.5+k2.*p_nh3(i);
%    r_h2(i)=3*(-k1.*p_n2(i).^0.5.*p_h2(i).^1.5+k2.*p_nh3(i));
%    r_nh3(i)=2*(+k1.*p_n2(i).^0.5.*p_h2(i).^1.5-k2.*p_nh3(i));
%    
%    f_n2(i+1)=f_n2(i)+r_n2(i)*V/n_step;
%    f_h2(i+1)=f_h2(i)+r_h2(i)*V/n_step;
%    f_nh3(i+1)=f_nh3(i)+r_nh3(i)*V/n_step;
%end
%x=linspace(0,V, n_step+1);
%plot (x,f_n2);



[V,f]=ode45(@(V,f)mass_balance(V,f,k1,k2,p,f_tot), intV, init)

plot(intV, f(1))


%___________________________________________
%%% ENERGY BALANCE PFR
%1=n2
%2=h2
%3=nh3

%dT(1)=(U*a*t-Ta)-r_n2*deltaH)/(Q*c_n2*Cp_n2+Q*c_h2*Cp_h2+Q*c_nh3*Cp_nh3);
%dT(2)=(U*a*t-Ta)-r_h2*deltaH)/(Q*c_n2*Cp_n2+Q*c_h2*Cp_h2+Q*c_nh3*Cp_nh3);
%dT(3)=(U*a*t-Ta)-r_nh3*deltaH)/(Q*c_n2*Cp_n2+Q*c_h2*Cp_h2+Q*c_nh3*Cp_nh3);

