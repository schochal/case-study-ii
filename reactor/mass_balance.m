function df = mass_balance(V,f,k1,k2,p,f_tot)
    df=zeros(3,1);
    df(1)=(-k1.*(p*f(1)/f_tot).^(0.5).*(p*f(2)/f_tot).^(1.5)+k2.*(p*f(3)/f_tot));
    df(2)=(3*(-k1.*(p*f(1)/f_tot).^(0.5).*(p*f(2)/f_tot).^(1.5)+k2.*(p*f(3)/f_tot)));
    df(3)=(2*(+k1.*(p*f(1)/f_tot).^(0.5).*(p*f(2)/f_tot).^(1.5)-k2.*(p*f(3)/f_tot)));
end


