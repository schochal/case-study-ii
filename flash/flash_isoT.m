function eqn=flash_isoT(unk)
global F z P0 Pf Tf

L=unk(1);
V=unk(2);
x=unk(3:6);
y=unk(7:10);

eqn(1)=F-L-V;     %[kmol/h]
eqn(2)=sum(x)- 1; %we could have used the Rachford Rice: sum(x)-sum(y) that is more stable!!
eqn(3:6)=F*z-L*x-V*y; %[kmol/h]
eqn(7:10)=P0(Tf,1:4).*x-Pf*y; %[atm] %phase equilibria with raoult first

end



