clc
clear all
close all

global F z P0 Pf Tf

%% Data

Tf=340; %[K]
Pf=50; %[bar]
F=100; %[kmol/h]
z=[.6878 .229 .0809 .0023];

    %    H2        N2        NH3       AR
AAnt=[ 3.54314    3.7362    4.86886   4.46903];
BAnt=[99.395    264.651  1113.928   481.012];
CAnt=[ 7.726     -6.788   -10.409    22.156];   %bar
P0=@(T,ii) 10.^(AAnt(ii)-BAnt(ii)./(T+CAnt(ii)));  %[bar], T[K]

check=P0(Tf,1:4);

%fmincon conditions
A = [];
B =[];
Aeq = [];
Beq = [];
lb = zeros(1,10);
ub = ones(1,10);
ub(1,1)=F;
ub(1,2)=F;

%% Flash 1 - Standard method
%      L   V    x1  x2  x3  x4  y1  y2  y3  y4
sol0=[F*0.99 F*0.01  0.0714 0.0495 0.8787 0.0003  0.6456 0.2167 0.1355 0.0022];  %first attempt solutions that respect
% the equations F=L+V ; sum(x)=1 and sum(y)=1. 

%sol=fsolve(@flash_isoT,sol0);
sol = fmincon(@flash_isoT, sol0, A, B, Aeq, Beq, lb, ub);

L=sol(1);
V=sol(2);
x=sol(3:6);
y=sol(7:10);

%% Flash 2 - Rachford-Rice
K=P0(Tf,1:4)/Pf;
eqRR=@(alfa) sum(z.*(1-K)./(1-alfa+alfa*K)); %only one equation where the unknown is alfa
alfa=fsolve(eqRR, 0.1); %NB alpha has to be between 0 and 1
V_RR=alfa*F;
L_RR=(1-alfa)*F;
x_RR=z./(1-alfa+alfa*K);
y_RR=K.*x;

%% display results
clc
fprintf(' x1  | %5.4f   |  %5.4f\n',x(1),x_RR(1))
fprintf(' x2  | %5.4f   |  %5.4f\n',x(2),x_RR(2))
fprintf(' x3  | %5.4f   |  %5.4f\n',x(3),x_RR(3))
fprintf(' x4  | %5.4f   |  %5.4f\n',x(4),x_RR(4))
fprintf(' y1  | %5.4f   |  %5.4f\n',y(1),y_RR(1))
fprintf(' y2  | %5.4f   |  %5.4f\n',y(2),y_RR(2))
fprintf(' y3  | %5.4f   |  %5.4f\n',y(3),y_RR(3))
fprintf(' y4  | %5.4f   |  %5.4f\n',y(4),y_RR(4))
fprintf(' L   | %5.4f  |  %5.4f kmol/h  \n',L,L_RR)
fprintf(' V   | %5.4f  |  %5.4f kmol/h  \n',V,V_RR)
