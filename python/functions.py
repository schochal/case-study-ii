#-*- coding: utf-8 -*-
# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
import scipy.optimize
from scipy.optimize import curve_fit
from scipy.stats import t
from scipy.integrate import odeint

from constants import *

#########################
# Actual Code starts here


#gas state constants
#0 = Hydrogen
#1 = Nitrogen
#2 = Ammonia
#3 = Argon


#########################
# Data

#               #0 = hydrogen, 1= nitrogen (100-500k),  2= ammonia, 3= argon, 4= nitrogen (500-2000K)
a = np.array([33.066178,    28.98641,	    19.99563,   20.78600,       19.50583])
b = np.array([-11.363417,	1.853978,       49.77119,	2.825911e-7,    19.88705,])
c = np.array([11.432816,    -9.647459,     -15.37599,  -1.464191e-7,  -8.598535,])
d = np.array([-2.772874,    16.63537,      1.921168,   1.092131e-8,     1.369784,])
e = np.array([-0.158558,    0.000117,      0.189174,   -3.661371e-8,    0.527601,])
f = np.array([-9.980797,    -8.671914,    -53.30667,	-6.197350,     -4.935202,])
g = np.array([172.707974,   226.4168,      203.8591,   179.9990,        212.3900,])
h = np.array([0.0,          0.0	,         -45.89806,  0.000000,         0.0])


tc = np.array([33.2, 126.2, 405.50, 150.86]) #critical temperature [K]
pc = 1e5 * np.array([13.0, 33.9, 112.5, 4.9]) #critical pressure [Pa]
omega = np.array([-0.220, 0.040, 0.253, 0]) #correction constant
zc = np.array([0.310841635, 0.287683845, 0.232920153, 0.029120378]) #correction compressability

r = 8.31446261815324 #universal gas constant



#Peng RobinsonEoS
def preos(V,T,P,index):
    #pc[index] = pc[index]*1e-5
    a = 0.45724*R*R*tc[index]*tc[index]/pc[index]
    b = 0.0778*R*tc[index]/pc[index]
    alpha = (1+(0.37464+1.54226*omega[index]-0.26922*omega[index]*omega[index])*(1-np.sqrt(T/tc[index])))**2
    #pc[index] = pc[index]*1e5
    #print('lalalla ', R*T/(V-b)-alpha*alpha/(V*V+2*b*V-b*b))
    return R*T/(V-b)-alpha*alpha/(V*V+2*b*V-b*b) - P

#compressibility factor
def z(T,V,P):
    Z= P*V/(r*T)
    return Z

#Patel-Teja-Valderrama-Cisternas EoS
def ptvc(V, T,P, Tc, Pc, Omega, Zc):
    oa = 0.69368018-1.0634424*Zc+0.68289995*Zc**2-0.21044403*Zc**3+0.003752658*Zc**4
    ob = 0.025987178+0.180754784*Zc+0.061258949*Zc**2
    oc = 0.577500514-1.898414283*Zc

    a = oa*r*r*Tc*Tc/Pc
    b = ob*r*Tc/Pc
    c = oc*r*Tc/Pc


    alpha = (1+(-6.608+70.43*Zc-159*Zc*Zc)*(1-np.sqrt(T/Tc)))**2
    #print('############################################################################')
    #print('inside: ', r*T/(V-b)-alpha*alpha/(V*V+b*V+c*V-c*b))
    #print('############################################################################')
    return - (r*T/(V-b)-alpha*alpha/(V*V+b*V+c*V-c*b)) + P

#test = z(500, 0.1, ptvc(500,0.1,tc[0],pc[0], omega[0], zc[0]))
#print(test)

#liquid ammonia heat capacity
def cp(T):
    a = 54.06
    b = -0.0085
    c = 278.052
    tc = 132.35
    Cp = A+B*T+C*(tc-T)**(-0.5)
    return Cp

#Ammonia enthalpy of vaporization
def hv(T):
    Hv = -5165.2*(T/1000)**3+4411.8*(T/1000)**2-1313.8*(T/1000)+156.25
    return Hv

#reaction rate of N2 ([mol/m³/s])
def rate(Ph2,Pn2,Pnh3,T):
    k1 = 1.0*10**4*np.exp(-9.1*10**4/r/T)
    k1 = 1.3*10**10*np.exp(-1.41*10**5/r/T)
    rateN2 = k1*(Ph2/atm)**1.5*(Pn2/atm)**0.5 - k2*(Pnh3/atm)
    return rateN2

#Departure functions for Peng–Robinson equation of state
#https://en.wikipedia.org/wiki/Departure_function
def depeq(y,T,V,Tc,Pc,Omega):
    P = preos(y,T,V,Tc,Pc,Omega)
    Z = z(T,V,P)

    k = 0.37464 + 1.54266*Omega - 0.26992*Omega*Omega
    Pr = P/Pc
    Tr = T/Tc
    B = 0.07780*Pr*Tr
    alpha = (1+(0.37464+1.54226*Omega-0.26922*Omega*Omega)*(1-np.sqrt(T/Tc)))**2


    dH = r*Tc*(Tr*(Z-1)-2.078 * (1+k)*np.sqrt(alpha)*np.log((Z+2.414*B)/(Z-0.414*B)))
    dS = r*(np.log(Z-B)- 2.078*k*((1+k)/np.sqrt(Tr)-k)*np.log((Z+2.414*B)/(Z-0.414*B)))

    return np.array([dH, dS])

#Enthalpy from ideal gas behaviour
def h_id(cp,Tin, Tout):
    H = cp*(Tout-Tin)
    return H

#Entropy from ideal gas behaviour
def s_id(cv, Tin, Tout, Vin, Vout):
    S = cv*np.log(Tout/Tin)+R*np.log(Vout/Vin)
    return S

#Nist webbook enthalpy calculations [J]
def enthalpy(i,T): #0 = hydrogen, 1= nitrogen (100-500k),  2= ammonia, 3= argon, 4= nitrogen (500-2000K)
    #If T is an array, we don't check for high temperature behaviour for the shomate equation for nitrogen
    if isinstance(T, (list, type(np.array([1,2])))):
        pass
    else:
        if isinstance(i, (list, type(np.array([1,2])))):
            i = np.array([ 4 if ((T > 500) and (j == 1)) else j for j in i ])
        else:
            i = 4 if ((T > 500) and (i == 1)) else i
    t = T * 1e-3
    #print('Enthalpy calculation for ', i ,' --> ', ( a[i] * t + b[i] * t*t/2 + c[i] * t*t*t/3 + d[i] * t*t*t*t/4 - e[i]/t + f[i] - h[i] ) * 1000)
    return ( a[i] * t + b[i] * t*t/2 + c[i] * t*t*t/3 + d[i] * t*t*t*t/4 - e[i]/t + f[i] - h[i] ) * 1000 # faster than t**n

def entropy(i,T): #0 = hydrogen, 1= nitrogen (100-500k),  2= ammonia, 3= argon, 4= nitrogen (500-2000K)
    if (isinstance(i, list) and not isinstance(T, list)):
        i = np.array([ 4 if ((T > 500) and (j == 1)) else j for j in i ])
    t = T * 1e-3
    return a[i] * np.log(t) + b[i] * t + c[i] * t*t/2 + d[i] * t*t*t/3 - e[i]/(t*t*2) + g[i] # faster than t**n

def heat_capacity(i, T): #0 = hydrogen, 1= nitrogen (100-500k),  2= ammonia, 3= argon, 4= nitrogen (500-2000K)
    if (isinstance(i, list) and not isinstance(T, list)):
        i = np.array([ 4 if ((T > 500) and (j == 1)) else j for j in i ])
    t = T * 1e-3
    print('Im here ?????????? ', t)
    return a[i] + b[i] * t + c[i] * t*t + d[i] * t*t*t + e[i]/(t*t) # faster than T**n


#heat exchange, A is the surface, T1 is in temp flow difference, T2 is cooling difference, U is the heat transfer coeff, Q is the needed energy to tranfer over the unit
def heat_exchanger(T1, T2, U, Q):
    mean = (T1-T2)/(np.log(T1)-np.log(T2))
    return Q / (U*mean) #this should be equal to A (from U*mean*A = Q --> A = Q/(mean*U))

# mcw = mass flow of cooling water, Tcw1 = cooling water inlet temperature, Tcw2 = cooling water outlet temperature, T_in = inlet tepmerautre, T_end = outlet temperature, feed_F = normalized array of flow, compounds = array of indices of involved substances
def cooling_water(mcw, Tcw1, Tcw2, T_in, T_end, feed_F, compounds):
    cp_cw= 4187 # [J/kg/K]
    Qcw = cp_cw*(Tcw2-Tcw1) * mcw
    #Qfeed = -feed_F[0] * enthalpy(0, T_end) \
    #    - feed_F[1] * enthalpy(1, T_end) \
    #    + feed_F[0] * enthalpy(0, T_in[0]) \
    #    + feed_F[1] * enthalpy(1, T_in[1])
    Qfeed = -sum(feed_F[compounds] * enthalpy(compounds, T_end)) + sum(feed_F[compounds] * enthalpy(compounds, T_in))
    return Qcw-Qfeed

def mass_water(Q, Tcw1, Tcw2):
    cp_cw= 4187 # [J/kg/K]
    return Q / cp_cw / (Tcw2 - Tcw1)

# mcw = mass flow of steam, Tcw1 = steam inlet temperature, Tcw2 = steam outlet temperature, T_in = inlet tepmerautre, T_end = outlet temperature, feed_F = normalized array of flow, compounds = array of indices of involved substances
def heating_steam(ms, Tsteam1, Tsteam2, T_in, T_end, feed_F, compounds):
    cp_steam= 1996 # [J/kg/K]
    Qcw = cp_steam*(Tsteam2-Tsteam1) * ms
    #Qfeed = -feed_F[0] * enthalpy(0, T_end) \
    #    - feed_F[1] * enthalpy(1, T_end) \
    #    + feed_F[0] * enthalpy(0, T_in[0]) \
    #    + feed_F[1] * enthalpy(1, T_in[1])
    Qfeed = -sum(feed_F[compounds] * enthalpy(compounds, T_end)) + sum(feed_F[compounds] * enthalpy(compounds, T_in))
    return Qcw-Qfeed

def heating_steam(Q, Tsteam1, Tsteam2):
    cp_steam= 1996 # [J/kg/K]
    return Q / cp_steam / (Tsteam1 - Tsteam2)

# [K] returns the heat production in a mixer for an output temperature T_end, a normalized (!) inlet flow array Fi and an input temperature array Ti. this should normally be 0 --> use the scipy.optimize.brentq() function to find its root


def eb_mixer(T_end, F1, Ti):
    return Fi[0] * enthalpy(0, T_end) \
        + Fi[1] * enthalpy(1, T_end) \
        - Fi[0] * enthalpy(0, Ti[0]) \
        - Fi[1] * enthalpy(1, Ti[1]) \
        #+ feed_F_normalized[2] * enthalpy(3, T_end) \
        #+ feed_F_normalized[3] * enthalpy(4, T_end) \
        #- feed_F_normalized[2] * enthalpy(3, feed_T[2]) \
        #- feed_F_normalized[3] * enthalpy(4, feed_T[3])


def eb_mixer_loop(T_end, F1, T1, F2, T2, index ):
    sumsum = sum(F1 * enthalpy(index, T_end)) \
        + sum(F2 * enthalpy(index, T_end)) \
        - sum(F1 * enthalpy(index, T1)) \
        - sum(F2 * enthalpy(index, T2)) \
        #+ feed_F_normalized[2] * enthalpy(3, T_end) \
        #+ feed_F_normalized[3] * enthalpy(4, T_end) \
        #- feed_F_normalized[2] * enthalpy(3, feed_T[2]) \
        #- feed_F_normalized[3] * enthalpy(4, feed_T[3])
    print('Im a Beeeeeee: sumsum =', sumsum)
    return sumsum


# returns the output temperature for a compressor for an input temperature T1, an inlet pressure P1, a desired output pressure P2 and a compound index using the Proposed Compressor Model
def eb_compressor(T1, P1, P2, feed_F_normalized, index):
    avg_heat_capacity = sum(feed_F_normalized[index] * heat_capacity(index, T1))
    gamma = avg_heat_capacity / (avg_heat_capacity - r)
    return T1 + T1 * ( ( P2 / P1 ) ** ((gamma - 1) / gamma) -1 ) / eta


#returns the work done by the compressor in [J/s = Watt], for an input temperature T1, an inlet pressure P1, a desired output pressure P2 and a compound index using the Proposed Compressor Model
def work_compressor(T1, P1, P2, index):
    avg_heat_capacity = sum(feed_F_normalized[index] * heat_capacity(index, T1))
    gamma = avg_heat_capacity / (avg_heat_capacity - r)

    W = r*T1*gamma/(gamma-1) * 1/eta  * ( (P2/P1)**((gamma-1)/gamma) - 1 )
    return W

#enthalpy flow
def enthalpy_flow(T,flow,i):
    ent= sum(flow * enthalpy(i, T))
    return ent
