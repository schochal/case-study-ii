# -*- coding: utf-8 -*-
# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from scipy.integrate import odeint, RK45



from functions import * # Import the functions file
from energy_balance import * # Import the energy_balance file
from reactor_asbi import * # Import the reactor file
from flash import * # Import the flash file
from constants import * #import the constants file

##########  Compressor H2 Feed  #############################################

T_H2_feed = feed_T[0]
P_H2_feed = feed_P[0]


T_H2 = eb_compressor(feed_T, feed_P, P_reactor*1e5, feed_H2 / sum(feed_H2), index)

#print('here',  T_H2)


Work_H2 = work_compressor(feed_T[0], feed_P[0], 1e5 * P_reactor, np.array([0])) * sum(feed_H2)

H_H2 = enthalpy_flow(T_H2, feed_F, [0])



##########  Compressor N2 Feed  #############################################
T_H2_feed = feed_T[1]
P_H2_feed = feed_P[1]

T_N2 = eb_compressor(feed_T, feed_P, P_reactor*1e5, feed_N2 / sum(feed_N2), index)


Work_N2 = work_compressor(feed_T[1],   feed_P[1], P_reactor * 1e5, index) * sum(feed_N2)
H_N2 = enthalpy_flow(T_N2, feed_N2, index)

print('gay ', feed_T[0], 'N2', feed_T[1])
##########  Mixer 1  ##############################################
# [K] Temperature at Point 4
T_4 = scipy.optimize.brentq(eb_mixer_loop, min(T_H2[0], T_N2[1]) - 1, max(T_H2[0], T_N2[1]) + 1, args=(feed_N2, T_N2[1], feed_H2, T_H2[0], index))

print('T_4 ', T_4 )

# [Pa] pressure at Point 4

# [J/s] Enthalpy at Point 4
H_4 = enthalpy_flow(T_4, feed_F, index)



######### Mixer 2/Start of Loop #####################################################

flow_recycle = np.array([202, 62.4, 50.8, 0.0])

T_recycle = T_flash

P_recycle = P_reactor

H_recycle = enthalpy_flow(T_recycle, flow_recycle, index)

scaleup_factor = 1

for j in range(0,100):

    print('==============================================================================')
    print('-------------------------- Iteration ' + str(j) + ' -----------------------------------')
    print('==============================================================================')
    flow = flow_recycle + feed_F
    flow_into_the_goddamn_reactor = flow
    #print('flow to the reactor: ', flow)
    #print('Ratio H2/N2 ', flow[0]/flow[1])

     #T_end = T_4
     #print(eb_mixer_loop(T_end, feed_F, T_4, flow_recycle, T_4, index ))

    #xarr = np.linspace(min(T_4, T_recycle) - 1, max(T_4, T_recycle) + 1, 100)
    #print([eb_mixer_loop(j,flow_recycle, T_recycle, feed_F, T_4, index) for j in xarr])

    T_5 = scipy.optimize.brentq(eb_mixer_loop, min(T_4, T_recycle) - 1, max(T_4, T_recycle) + 1, args=(flow_recycle, T_recycle, feed_F, T_4, index))
    print('T_5 ', T_5)

    P_5 = P_reactor

    H_5 = enthalpy_flow(T_5, flow, index)


    ################  Heat exchanger 1  #######################################################################


    T_6 = T_reactor

    P_6 = P_5

    H_6 = enthalpy_flow(T_6, flow, index)

    Q_HE1 = H_5 - H_6
    print('Q HE1: ',Q_HE1)

    if T_6 < T_5:
        A_HE1 = heat_exchanger(T_5 - cooling_water_outlet_T, T_6 - cooling_water_inlet_T, h_steel, Q_HE1)
        print('HE1: cooling')
        #m_heat_exchanger = scipy.optimize.brentq(cooling_water, 1, 1000, args=(cooling_water_inlet_T, cooling_water_outlet_T, T_5, T_6, flow, index))
        m_HE1 = mass_water(Q_HE1,cooling_water_inlet_T, cooling_water_outlet_T)
        #print('mass flow rate heat exchagner: ', m_heat_exchanger)


    if T_6 > T_5:
        A_HE1 = heat_exchanger(T_5 - steam_outlet_T, T_6 - steam_inlet_T, h_steel, Q_HE1)
        print('HE 1: heating')
        #m_heat_exchanger = scipy.optimize.brentq(heating_steam, 1, 1000, args=(steam_inlet_T, steam_outlet_T, T_5, T_6, flow, index))
        m_HE1 = heating_steam(Q_HE1,steam_inlet_T, steam_outlet_T)
        #print('mass flow rate heat exchagner: ', m_heat_exchanger)




    ################  Reactor  #######################################################################

    print('flow: ', flow)
    in_reactor = flow

    dx= np.linspace(0,V_reactor,100000) #dimensionless length of the film, 10'000 steps

    out_reactor = odeint(reactor, in_reactor,dx, args=(T_reactor, P_reactor, V_reactor))

    flow = out_reactor[-1,:]
    flow_out_off_the_godamn_reactor = flow

    H_7 = enthalpy_flow(T_reactor, flow, index)

    Q_reactor = H_6 - H_7

    print('super Perforator: ', 2*enthalpy(2, T_reactor)-3*enthalpy(0, T_reactor)-enthalpy(1, T_reactor))


    A_HE_reactor = heat_exchanger(T_reactor - cooling_water_outlet_T, T_reactor - cooling_water_inlet_T, h_steel, Q_reactor)
    #print(A_HE_reactor)

    #TODO: mass water reactor
    m_cw_reactor = mass_water(Q_reactor,cooling_water_inlet_T, cooling_water_outlet_T)
    #print('conversion: ', (in_reactor-flow)/in_reactor)


    ################  Heat exchanger 2  #######################################################################


    T_8 = T_flash

    P_8 = P_flash

    H_8 = enthalpy_flow(T_8, flow, index)

    Q_HE2 = H_7 - H_8 - hv(T_flash)*flow[2]
    print('Q HE2: ',Q_HE2)

    if T_reactor > T_flash:
        A_HE2 = heat_exchanger(T_reactor - cooling_water_outlet_T, T_8 - cooling_water_inlet_T, h_steel, Q_HE2)
        #print('cooling: ', A_HE2)
        #xarr = np.linspace(1,1000,100)
        #print([mass_water(j, cooling_water_inlet_T, cooling_water_outlet_T, T_flash, T_8, flow, index) for j in xarr])
        #m_heat_exchanger = scipy.optimize.brentq(cooling_water, 1, 1000, args=(cooling_water_inlet_T, cooling_water_outlet_T, T_flash, T_8, flow, index))
        m_HE2 = mass_water(Q_HE2,cooling_water_inlet_T, cooling_water_outlet_T)
        #print('mass water: ', m_heat_exchanger)
    else:
        A_HE2 = heat_exchanger(T_reactor - steam_outlet_T, T_8 - steam_inlet_T, h_steel, Q_HE2)
        #print('heating: ', A_HE2)
        #m_heat_exchanger = scipy.optimize.brentq(heating_steam, 1, 1000, args=(steam_inlet_T, steam_outlet_T, T_flash, T_8, flow, index))
        m_HE2 = heating_steam(Q_HE2,steam_inlet_T, steam_outlet_T)
        #print('mass steam: ', m_heat_exchanger)


        #xarr = np.linspace(1,10000,100)
        #print([heating_steam(j, steam_inlet_T, steam_outlet_T, T_flash, T_8, flow, index) for j in xarr])



    ################  Flash #######################################################################


    #print('composition: ', flow / sum(flow))
    xarr = np.linspace(0.0,1.0,100)
    #print([Ra_Rice(j, flow, P_flash, T_flash, index) for j in xarr])
    alpha_opt = scipy.optimize.brentq(Ra_Rice, 0, 1, args=(flow, P_flash, T_flash, index))

    #print(alpha_opt)

    production_stream = flash_flow_x(alpha_opt, flow, P_flash, T_flash, index)
    recycle_stream    = flash_flow_y(alpha_opt, flow, P_flash, T_flash,index)

    production_composition = flash_x(alpha_opt, flow, P_flash, T_flash, index) / sum(flow)
    recycle_composition    = flash_y(alpha_opt, flow, P_flash, T_flash,index) / sum(flow)

    H_9  = enthalpy_flow(T_flash, production_stream, index)
    H_10 = enthalpy_flow(T_flash, recycle_stream, index)
    Q_flash = - hv(T_flash) * production_stream[2]

    xarr = np.linspace(.005 * R * T_flash / (P_flash* 1e5), 5 * R * T_flash / (P_flash* 1e5), 100) #2.8*-5

    #print('!!!  ', .005 * R * T_flash / (P_flash* 1e5), 5 * R * T_flash / (P_flash* 1e5))
    #print([preos(j, T_flash, P_flash* 1e5, index) for j in xarr])

    #V_flash = np.array([scipy.optimize.brentq(preos, .005 * R * T_flash / (P_flash* 1e5), 5 * R * T_flash / (P_flash* 1e5), args=(T_flash, P_flash* 1e5, index[i])) for i in np.array([0,1,2])])
    V_flash = np.array([1 / 1928.1, 1 / 1984.3, 1 / 35183, 1 / 2036.2]) # [kg / m**3]
    #print('try ry y... why?', V_flash)


    #xarr = np.linspace(.045 * R * T_flash / (P_flash* 1e5), .05 * R * T_flash / (P_flash* 1e5), 10)
    #print(xarr)
    #print([ptvc(j, T_flash, P_flash,tc[2],pc[2],omega[2],zc[2]) for j in xarr])
    #V_liquid = scipy.optimize.brentq(ptvc, 0.045 * R * T_flash / (P_flash* 1e5), .05 * R * T_flash / (P_flash* 1e5), args=(T_flash, P_flash,tc[2],pc[2],omega[2],zc[2]))
    V_liquid = 1 / 35185
    #print('??? ', V_liquid)
    #TODO: find v_in and density of the out flows

    ################  Spilt  #######################################################################

    recycle = recycle_stream * ( 1 - purge )


    ################  Compressor 2  #######################################################################

    T_13 = eb_compressor(T_flash, P_flash * 1e5, P_reactor*1e5, recycle / sum(recycle), index)
    print('T_13 ', T_13)

    P_13 = P_flash

    Work_c2 = work_compressor(T_flash, P_flash, P_reactor, index) * sum(recycle)

    H_13 = enthalpy_flow(T_13, recycle, index)

    #print(Work_c2, T_13)


    T_recycle = T_13

    P_recycle = P_reactor

    H_recycle = H_13

    scaleup_factor = 186 / production_stream[2]

    print('HE1:    ', A_HE1, ', mass flow water: ' , m_HE1)
    print('HE2:    ', A_HE2, ', mass flow water: ' , m_HE2)
    #print('WC H2:    ', Work_H2, 'WC N2: ', Work_N2, 'WC recycle: ', Work_c2)
    #print('Vflash: ', V_flash)

    print('================ Results ================')
    print('Production: ', production_stream)
    print('Purge:      ', recycle_stream * purge)
    print('Recycle:    ', recycle)
    print('Sacleup F.: ', scaleup_factor)
    #ideal case: 186 mols/s to meet production


    mass_vapour_stream = sum(recycle_stream * molar_mass)
    mass_liquid_stream = sum(production_stream * molar_mass)

    volume_vapour_stream = sum(recycle_stream * V_flash)
    volume_liquid_stream = production_stream[2] * V_liquid

    print()

    avg_density = 1 / sum(recycle_composition * V_flash)

    flow_recycle = recycle


V_liquid =  (35185)**(-1)

F = open('results_corr_' + str(V_reactor) + 'm3_' + str(T_reactor) + 'K.txt','w')

F.write('Feed [kg s-1]:       ' + str(feed_F * molar_mass) + '\n')
F.write('Work Comp H2 [W]:    ' + str(Work_H2)+'\n')
F.write('Work Comp N2 [W]:    ' + str(Work_N2)+'\n')
F.write('Area HE1 [m2]:       ' + str(A_HE1) + ', mass flow water [kg s-1]: ' + str(m_HE1) + '  Q: ' +str(Q_HE1) +'\n')
F.write('flow in [kg s-1]:    ' + str(flow_into_the_goddamn_reactor * molar_mass) + '\n')
F.write('Area reactor HE [m2]:' + str(A_HE_reactor) + ', mass flow water [kg s-1]: ' + str(m_cw_reactor) + '  Q: ' +str(Q_reactor) + '\n')
F.write('Area HE2 [m2]:       ' + str(A_HE2) + ', mass flow water [kg s-1]: ' + str(m_HE2) + '\n')
F.write('Work Comp 2 [W]:     ' + str(Work_c2)+'\n')
F.write('Flash Vap [kg s-1]:  ' + str(mass_vapour_stream) + ', Flash liq [kg s-1]: ' + str(mass_liquid_stream) + '  Q: ' +str(Q_HE2) + '\n')
F.write('Feed Flash [m3 s-1]: ' + str(sum(flow * V_flash)) + '\n')
F.write('Q Flash [W]:         ' + str(Q_flash) + '\n')

F.write('\n')
F.write('================ Results ================\n')
F.write('Production [kg s-1]: ' + str(production_stream * molar_mass) + '\n')
F.write('Purge [kg s-1]:      ' + str(recycle_stream * purge * molar_mass) + '\n')
F.write('Conv. H2:            ' + str((flow_into_the_goddamn_reactor[0] - flow[0]) / flow_into_the_goddamn_reactor[0]) + '\n')
F.write('Molar Yield:         ' + str( production_stream[2] * 3 / feed_F[0] / 2) + '\n')
F.write('Purity               ' + str(production_stream[2]/sum(production_stream)) + '\n')
F.write('Scaleup factor       ' + str(scaleup_factor) + '\n')
#F.write('Sacleup F.: ' + str(scaleup_factor) + '\n')


F.write('================ Values =================\n')
F.write('Stage       ' + 'Temperature [K]         ' + 'Pressure [bar]           ' + 'molar Flows [mol s-1], [H2 N2 NH3 Ar]' + '\n')
F.write('Feed H2    :' + str(feed_T[0]) +'                   ' +str(feed_P[0]*1e-5) +'       '+ str(feed_H2) + '\n')
F.write('Feed N2    :' + str(feed_T[1]) +'                   ' +str(feed_P[1]*1e-5) +'        '+ str(feed_N2) + '\n')
F.write('Comp. H2   :' + str(T_H2[0])   +'       '+ str(P_reactor) +'                      '+ str(feed_H2) + '\n')
F.write('Comp. N2   :' + str(T_N2[1])   +'       '+str(P_reactor)  +'                      '+ str(feed_N2) + '\n')
F.write('4          :' + str(T_4)       +'           '+ str(P_reactor) +'                      '+ str(feed_F) + '\n')
F.write('5          :' + str(T_5)       +'           '+ str(P_reactor) +'                      '+ str(flow_into_the_goddamn_reactor) + '\n')
F.write('6          :' + str(T_6)       +'                     '+ str(P_reactor) +'                      '+ str(flow_into_the_goddamn_reactor) + '\n')
F.write('7          :' + str(T_6)       +'                     '+ str(P_reactor) +'                      '+ str(flow_out_off_the_godamn_reactor) + '\n')
F.write('8          :' + str(T_flash)   +'                     '+ str(P_flash)   +'                       '+ str(flow_out_off_the_godamn_reactor) + '\n')
F.write('9          :' + str(T_flash)   +'                     '+ str(P_flash)   +'                       '+ str(production_stream) + '\n')
F.write('10         :' + str(T_flash)   +'                     '+ str(P_flash)   +'                       '+ str(recycle_stream) + '\n')
F.write('11         :' + str(T_flash)   +'                     '+ str(P_flash)   +'                       '+ str(recycle_stream * purge) + '\n')
F.write('12         :' + str(T_flash)   +'                     '+ str(P_flash)   +'                       '+ str(recycle) + '\n')
F.write('13         :' + str(T_13)      +'       '+ str(P_reactor) +'                      '+ str(recycle) + '\n')

F.close()
