# -*- coding: utf-8 -*-
# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from scipy.integrate import odeint, RK45



from functions import * # Import the functions file
from energy_balance import * # Import the energy_balance file
from reactor_asbi import * # Import the reactor file
from flash import * # Import the flash file


##########  Mixer 1  ##############################################
# [K] Temperature at Point 3
T_3 = scipy.optimize.brentq(eb_mixer, min(feed_T[0], feed_T[1]) - 1, max(feed_T[0], feed_T[1]) + 1, args=(feed_F_normalized, feed_T))

# [Pa] pressure at Point 3
P_3 = np.amin(feed_P)

# [J/s] Enthalpy at Point 3
H_3 = enthalpy_flow(tc, feed_F, index)


##########  Compressor 1  #############################################
T_4 = eb_compressor(T_3, P_3, P_reactor*1e5, feed_F_normalized, index)
#print('T_4 ', T_4)
P_4 = P_reactor

Work_c1 = work_compressor(T_3, P_3, P_reactor, index)

H_4 = enthalpy_flow(T_4, feed_F, index)


######### Mixer 2/Start of Loop #####################################################

flow_recycle = np.array([202, 62.4, 50.8, 0.0001])

T_recycle = T_flash

P_recycle = P_reactor

H_recycle = enthalpy_flow(T_recycle, flow_recycle, index)

for j in range(0,1):

    print('==============================================================================')
    print('-------------------------- Iteration ' + str(j) + '---------------------------------')
    print('==============================================================================')
    flow = flow_recycle + feed_F
    print('flow: ', flow)


     #T_end = T_4
     #print(eb_mixer_loop(T_end, feed_F, T_4, flow_recycle, T_4, index ))

    #xarr = np.linspace(min(T_4, T_recycle) - 1, max(T_4, T_recycle) + 1, 100)
    #print([eb_mixer_loop(j,flow_recycle, T_recycle, feed_F, T_4, index) for j in xarr])

    T_5 = scipy.optimize.brentq(eb_mixer_loop, min(T_4, T_recycle) - 1, max(T_4, T_recycle) + 1, args=(flow_recycle, T_recycle, feed_F, T_4, index))
    print('t_5 intervall ', min(T_4, T_recycle) - 1, max(T_4, T_recycle) + 1)
    print('T_5 ', T_5)

    P_5 = P_reactor

    H_5 = enthalpy_flow(T_5, flow, index)


    ################  Heat exchanger 1  #######################################################################


    T_6 = T_reactor

    P_6 = P_5

    H_6 = enthalpy_flow(T_6, flow, index)

    Q_HE1 = H_6 - H_5

    if T_6 < T_5:
        A_HE1 = heat_exchanger(T_5 - cooling_water_outlet_T, T_6 - cooling_water_inlet_T, h_steel, Q_HE1)
        print('cooling', A_HE1)
        #m_heat_exchanger = scipy.optimize.brentq(cooling_water, 1, 1000, args=(cooling_water_inlet_T, cooling_water_outlet_T, T_5, T_6, flow, index))
        m_heat_exchanger = mass_water(Q_HE1,cooling_water_inlet_T, cooling_water_outlet_T)
        print(m_heat_exchanger)


    if T_6 > T_5:
        A_HE1 = heat_exchanger(T_5 - steam_outlet_T, T_6 - steam_inlet_T, h_steel, Q_HE1)
        #print('heating', A_HE1)
        #m_heat_exchanger = scipy.optimize.brentq(heating_steam, 1, 1000, args=(steam_inlet_T, steam_outlet_T, T_5, T_6, flow, index))
        m_heat_exchanger = heating_steam(Q_HE1,steam_inlet_T, steam_outlet_T)
        print(m_heat_exchanger)




    ################  Reactor  #######################################################################

    #print('flow: ', flow)
    in_reactor = flow

    dx= np.linspace(0,V_reactor,100000) #dimensionless length of the film, 10'000 steps

    out_reactor = odeint(reactor, in_reactor,dx, args=(T_reactor, P_reactor, V_reactor))

    flow = out_reactor[-1,:]

    H_7 = enthalpy_flow(T_reactor, flow, index)

    Q_reactor = H_7 - H_6

    A_HE_reactor = heat_exchanger(T_reactor - cooling_water_outlet_T, T_reactor - cooling_water_inlet_T, h_steel, Q_reactor)
    print(A_HE_reactor)

    print('conversion: ', (in_reactor-flow)/in_reactor)
    print(' flow after reactor: ', flow)


    flow_recycle = flow
