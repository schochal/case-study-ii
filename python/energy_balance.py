#-*- coding: utf-8 -*-
# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
import scipy.optimize
#import sympy as sp
#from pint import UnitRegistry
from scipy.optimize import curve_fit
from scipy.stats import t
from scipy.integrate import odeint
#ureg = UnitRegistry()

from constants import *
from functions import *


##################################################
# Constants

lambda_steel = 55 # [W/K/m]
#steel_thickness = ((1 * ureg.inch).to(ureg.m).magnitude)
h_steel = lambda_steel / steel_thickness

cooling_water_inlet_T = 283.15 # [K]
cooling_water_outlet_T = 287.15 # [K]

# Feed into the plant
feed_T = np.array([298.0, 398.0, 298.0, 298.0]) # [K], H2 N2 NH3 Ar
feed_F = np.array([437.0, 146.0, 0.0, 0.0]) # [mol/s], H2 N2 NH3 Ar
feed_P = np.array([3e6, 7e5, 7e5, 7e5]) # [Pa], H2 N2 NH3 Ar
feed_F_normalized = feed_F / sum(feed_F)


##################################################
# Calculations



# [K] Temperature at Point 3
#T_3 = scipy.optimize.brentq(eb_mixer, min(feed_T[0], feed_T[1]) - 1, max(feed_T[0], feed_T[1]) + 1, args=(feed_F_normalized, feed_T))

# [K] Temperature at Point 4 using the compressor model above, for each chemical individually
#T_4 = eb_compressor(T_3, min(feed_P), P_compressor, feed_F_normalized, np.array([0,1,]))

# Required heat to be removed for the heat exchanger 1
#Q = sum(feed_F[np.array([0,1])] * enthalpy(np.array([0,1]), T_4)) - sum(feed_F[np.array([0,1])] * enthalpy(np.array([0,1]), T_reactor)) # [W]

# Required mass flow of water for the heat exchaner 1
#m_water = scipy.optimize.brentq(cooling_water, 1, 1000, args=(cooling_water_inlet_T,cooling_water_outlet_T,T_4,T_reactor,feed_F,np.array([0,1])))

# Required area of the heat exchanger 1
#A_HE1 = heat_exchanger(T_4 - cooling_water_outlet_T, T_reactor - cooling_water_inlet_T, h_steel, Q)
