# -*- coding: utf-8 -*-
# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from scipy.integrate import odeint, RK45



from functions import * # Import the functions file
from energy_balance import * # Import the energy_balance file
from reactor_asbi import * # Import the reactor file
from flash import * # Import the flash file
from constants_butheatexchangerisactuallyaflash import *


##########  Mixer 1  ##############################################
# [K] Temperature at Point 3
T_3 = scipy.optimize.brentq(eb_mixer, min(feed_T[0], feed_T[1]) - 1, max(feed_T[0], feed_T[1]) + 1, args=(feed_F_normalized, feed_T))

# [Pa] pressure at Point 3
P_3 = np.amin(feed_P)

# [J/s] Enthalpy at Point 3
H_3 = enthalpy_flow(tc, feed_F, index)


##########  Compressor 1  #############################################
T_4 = eb_compressor(T_3, P_3, P_reactor*1e5, feed_F_normalized, index)
print('!! !! !! ',T_4)
P_4 = P_reactor

Work_c1 = work_compressor(T_3, P_3 / 1e5, P_reactor, index)

H_4 = enthalpy_flow(T_4, feed_F, index)


######### Mixer 2/Start of Loop #####################################################

flow_recycle = np.array([202, 62.4, 50.8, 0.0])

T_recycle = T_flash

P_recycle = P_reactor

H_recycle = enthalpy_flow(T_recycle, flow_recycle, index)

scaleup_factor = 1

for j in range(0,100):

    print('==============================================================================')
    print('-------------------------- Iteration ' + str(j) + ' -----------------------------------')
    print('==============================================================================')
    flow = flow_recycle + feed_F
    flow_into_the_goddamn_reactor = flow
    #print('flow to the reactor: ', flow)
    #print('Ratio H2/N2 ', flow[0]/flow[1])

     #T_end = T_4
     #print(eb_mixer_loop(T_end, feed_F, T_4, flow_recycle, T_4, index ))

    #xarr = np.linspace(min(T_4, T_recycle) - 1, max(T_4, T_recycle) + 1, 100)
    #print([eb_mixer_loop(j,flow_recycle, T_recycle, feed_F, T_4, index) for j in xarr])

    T_5 = scipy.optimize.brentq(eb_mixer_loop, min(T_4, T_recycle) - 1, max(T_4, T_recycle) + 1, args=(flow_recycle, T_recycle, feed_F, T_4, index))
    print('LOLOLOLOLOLOLOL ', T_5, ' LOLOLOLOLOLOLOL ')

    P_5 = P_reactor

    H_5 = enthalpy_flow(T_5, flow, index)


    ################  Heat exchanger 1  #######################################################################


    T_6 = T_reactor

    P_6 = P_5

    H_6 = enthalpy_flow(T_6, flow, index)

    Q_HE1 = H_5 - H_6
    print('Q HE1: ',Q_HE1)

    if T_6 < T_5:
        A_HE1 = heat_exchanger(T_5 - cooling_water_outlet_T, T_6 - cooling_water_inlet_T, h_steel, Q_HE1)
        print('HE1: cooling')
        #m_heat_exchanger = scipy.optimize.brentq(cooling_water, 1, 1000, args=(cooling_water_inlet_T, cooling_water_outlet_T, T_5, T_6, flow, index))
        m_HE1 = mass_water(Q_HE1,cooling_water_inlet_T, cooling_water_outlet_T)
        #print('mass flow rate heat exchagner: ', m_heat_exchanger)


    if T_6 > T_5:
        A_HE1 = heat_exchanger(T_5 - steam_outlet_T, T_6 - steam_inlet_T, h_steel, Q_HE1)
        print('HE 1: heating')
        #m_heat_exchanger = scipy.optimize.brentq(heating_steam, 1, 1000, args=(steam_inlet_T, steam_outlet_T, T_5, T_6, flow, index))
        m_HE1 = heating_steam(Q_HE1,steam_inlet_T, steam_outlet_T)
        #print('mass flow rate heat exchagner: ', m_heat_exchanger)




    ################  Reactor  #######################################################################

    print('flow: ', flow)
    in_reactor = flow

    dx= np.linspace(0,V_reactor,100000) #dimensionless length of the film, 10'000 steps

    out_reactor = odeint(reactor, in_reactor,dx, args=(T_reactor, P_reactor, V_reactor))

    flow = out_reactor[-1,:]

    H_7 = enthalpy_flow(T_reactor, flow, index)

    Q_reactor = H_6 - H_7

    A_HE_reactor = heat_exchanger(T_reactor - cooling_water_outlet_T, T_reactor - cooling_water_inlet_T, h_steel, Q_reactor)
    #print(A_HE_reactor)

    #print('conversion: ', (in_reactor-flow)/in_reactor)


    ################  Heat exchanger 2  #######################################################################


    T_8 = T_flash

    P_8 = P_flash

    H_8 = enthalpy_flow(T_8, flow, index)

    #
    alpha_opt = scipy.optimize.brentq(Ra_Rice, 0, 1, args=(flow, P_reactor, T_flash, index))

    liquid_stream = flash_flow_x(alpha_opt, flow, P_reactor, T_flash, index)
    vapour_stream = flash_flow_y(alpha_opt, flow, P_reactor, T_flash,index)

    liquid_composition = flash_x(alpha_opt, flow, P_reactor, T_flash, index) / sum(flow)
    vapour_composition = flash_y(alpha_opt, flow, P_reactor, T_flash, index) / sum(flow)

    flow_HE2 = flash_flow_x(alpha_opt, flow, P_reactor, T_flash, index)

    comp_HE2 = flash_x(alpha_opt, flow, P_reactor, T_flash, index) / sum(flow)

    print('liquid_composition: ', liquid_composition)

    H_9  = enthalpy_flow(T_flash, liquid_stream, index)

    Q_flash = - hv(T_flash) * liquid_stream[2]

    V_flash_in = np.array([1 / 4502.4, 1 / 4426.4, 1 / 7414.2, 1 / 4627.7])
    V_flash_out = np.array([1 / 7070.2, 1 / 7497.3, 1 / 35961, 1 / 8290.0])
    V_liquid = 1 / 35185

    avg_volume_in = sum(flow * V_flash_in)
    avg_density_out_vapour = 1 / sum(vapour_composition * V_flash_in)
    avg_density_out_liquid = 1 / sum(liquid_composition * V_flash_in)

    Q_HE2 = H_7 - H_8 - Q_flash

    print('Q HE2: ',Q_HE2)

    print('flow v: ', vapour_stream)
    print('flow l: ', liquid_stream)

    if T_reactor > T_flash:
        A_HE2 = heat_exchanger(T_reactor - cooling_water_outlet_T, T_8 - cooling_water_inlet_T, h_steel, Q_HE2)
        m_HE2 = mass_water(Q_HE2,cooling_water_inlet_T, cooling_water_outlet_T)
    else:
        A_HE2 = heat_exchanger(T_reactor - steam_outlet_T, T_8 - steam_inlet_T, h_steel, Q_HE2)
        m_HE2 = heating_steam(Q_HE2,steam_inlet_T, steam_outlet_T)

    ################  Flash #######################################################################

    print('flow in: ', liquid_stream)

    alpha_opt = scipy.optimize.brentq(Ra_Rice, 0, 1, args=(liquid_stream, P_flash, T_flash, index))


    production_stream = flash_flow_x(alpha_opt, liquid_stream, P_flash, T_flash, index)
    recycle_stream    = flash_flow_y(alpha_opt, liquid_stream, P_flash, T_flash,index)


    production_composition = flash_x(alpha_opt, liquid_stream, P_flash, T_flash, index) / sum(flow)
    recycle_composition    = flash_y(alpha_opt, liquid_stream, P_flash, T_flash,index) / sum(flow)

    H_9  = enthalpy_flow(T_flash, production_stream, index)
    H_10 = enthalpy_flow(T_flash, recycle_stream, index)

    Q_flash = - hv(T_flash) * production_stream[2]

    V_flash = np.array([1 / 1928.1, 1 / 1984.3, 1 / 35183, 1 / 2036.2]) # [kg / m**3]
    V_liquid = 1 / 35185

    ################  Spilt  #######################################################################

    recycle = recycle_stream * ( 1 - purge )


    ################  Compressor 2  #######################################################################

    T_13 = eb_compressor(T_flash, P_flash * 1e5, P_reactor*1e5, recycle / sum(recycle), index)

    P_13 = P_flash

    Work_c2 = work_compressor(T_flash, P_flash, P_reactor, index)

    H_13 = enthalpy_flow(T_13, recycle, index)

    #print(Work_c2, T_13)

    ###############  Mixer 3  ###########################################################################

    #print('!!!!!!!!!!!! ', T_13)
    T_14 = scipy.optimize.brentq(eb_mixer_loop, min(T_13, T_flash) - 1, max(T_13, T_flash) + 1, args=(vapour_stream, T_flash, recycle, T_13, index))
    print('T_5 ', T_5)

    P_14 = P_reactor

    flow = vapour_stream + recycle

    H_14 = enthalpy_flow(T_14, flow, index)







    ############### END of Loop  ########################################################################
    T_recycle = T_14

    P_recycle = P_reactor

    H_recycle = H_14

    flow_recycle = recycle + vapour_stream

    scaleup_factor = 186 / production_stream[2]

    print('HE1:    ', A_HE1, ', mass flow water: ' , m_HE1)
    print('HE2:    ', A_HE2, ', mass flow water: ' , m_HE2)
    print('WC1:    ', Work_c1, 'WC2: ', Work_c2)
    #print('Vflash: ', V_flash)

    print('================ Results ================')
    print('Production: ', production_stream)
    print('Purge:      ', recycle_stream * purge)
    print('Sacleup F.: ', scaleup_factor)
    #ideal case: 186 mols/s to meet production


    avg_density = 1 / sum(recycle_composition * V_flash)



print('V_liquid ',V_liquid)

F = open('Try_results_' + str(V_reactor) + 'm3_' + str(T_reactor) + 'K.txt','w')

F.write('Feed [kg s-1]:               ' + str(feed_F * molar_mass) + '\n')
F.write('Area HE1 [m2]:               ' + str(A_HE1) + ', mass flow water [kg s-1]: ' + str(m_HE1) + '\n')
F.write('flow in [kg s-1]:            ' + str(flow_into_the_goddamn_reactor * molar_mass) + '\n')
F.write('Area HE2 [m2]:               ' + str(A_HE2) + ', mass flow water [kg s-1]: ' + str(m_HE2) + '\n')
F.write('Work Comp1 [W]:              ' + str(Work_c1)+ ', WC2 [W]: '+ str(Work_c2) + '\n')
F.write('HE2 V in [m3 s-1]:           ' + str(avg_volume_in) + '\n')
F.write('HE2 avg rho vapour [kg m-3]: ' + str(avg_density_out_vapour) + '\n')
F.write('HE2 avg rho liquid [kg m-3]: ' + str(avg_density_out_liquid) + '\n')
#F.write('Rho V [mol m-3]:             ' + str(avg_density) + ', Rho L [mol m-3]: ' + str(1 / V_liquid) + '\n')
F.write('Feed Flash [m3 s-1]:         ' + str(sum(flow * V_flash)) + '\n')


F.write('================ Results ================\n')
F.write('Production [kg s-1]: ' + str(production_stream * molar_mass) + '\n')
F.write('Purge [kg s-1]:      ' + str(recycle_stream * purge * molar_mass) + '\n')
F.write('Conv. H2:            ' + str((flow_into_the_goddamn_reactor[0] - flow[0]) / flow_into_the_goddamn_reactor[0]) + '\n')
F.write('Molar Yield:         ' + str( production_stream[2] * 3 / feed_F[0] / 2) + '\n')
F.write('Purity               ' + str(production_stream[2]/sum(production_stream)) + '\n')
F.write('Scaleup factor       ' + str(scaleup_factor) + '\n')

F.close()
