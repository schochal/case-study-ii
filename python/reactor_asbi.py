# -*- coding: utf-8 -*-
# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from scipy.integrate import odeint, RK45

plt.rc('text.latex', preamble=r'\usepackage{siunitx}')


from functions import * # Import the functions file
from constants import *

# Constants
c0 = np.array([4.08579833e+02, 1.36652693e+02, 4.80916620e+01, 1.48301214e-03]) #np.array([1861, 621, 0, 0])


T = T_reactor #773

V = V_reactor # 15.36
p = P_reactor #200

atm = 101325

k1 = 1.0e4*np.exp(-9.1e4/(R*T))
k2 = 1.3e10*np.exp(-1.41e5/(R*T))

print(k1/k2)

def reactor(c,x,T,p,V):

    f_tot = sum(c)
    c_h2 = c[0]
    c_n2 = c[1]
    c_nh3= c[2]
    c_ar = c[3]


    k1 = 1.0e4*np.exp(-9.1e4/(r*T))
    k2 = 1.3e10*np.exp(-1.41e5/(r*T))

    dc_h2=1000*3*(-k1*(p*c[0]/f_tot)**(0.5)*(p*c[1]/f_tot)**(1.5)+k2*(p*c[2]/f_tot));
    dc_n2=1000*((-k1*(p*c[0]/f_tot)**(0.5)*(p*c[1]/f_tot)**(1.5)+k2*(p*c[2]/f_tot)));
    dc_nh3=1000*2*((+k1*(p*c[0]/f_tot)**(0.5)*(p*c[1]/f_tot)**(1.5)-k2*(p*c[2]/f_tot)));
    dc_ar = 0

    return [dc_h2, dc_n2, dc_nh3, dc_ar]

#print(conc[cain,cbin,ccin,dadx_in,dbdx_in,dcdx_in],l)
dx= np.linspace(0,V,100000) #dimensionless length of the film, 10'000 steps


solution = odeint(reactor,c0,dx, args=(T,p,V))

u_h2 = solution[:,0]
u_n2 = solution[:,1]
u_nh3 = solution[:,2]
u_ar  = solution[:,3]

print(u_h2[-1], u_n2[-1], u_nh3[-1] , u_ar[-1] )

plt.plot(dx,u_h2, linestyle='-', color='black', label=r'c\textrm{H\textsubscript{2}}', linewidth=1)
plt.plot(dx,u_n2, linestyle=':', color='black', label=r'c\textrm{N\textsubscript{2}}', linewidth=1)
plt.plot(dx,u_nh3, linestyle='-.', color='black', label=r'c\textrm{NH\textsubscript{3}}', linewidth=1)

plt.xlabel(r'$V$ / \si{\cubic\meter}', fontsize=16)
plt.ylabel(r'$c$ / \si{\mol\per\cubic\meter}', fontsize=16)



plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)
plt.tight_layout()
plt.legend()
plt.savefig("reactor.pdf")
