# -*- coding: utf-8 -*-
# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from scipy.integrate import odeint, RK45

from functions import * # Import the functions file


##################################################
# Constants In

# composition in the beginning
z = np.array([0.34170173, 0.11390058, 0.54439769, 0.000])


# total pressure
Ptot = 150 # [bar]

# temperature
T = 303 # [K]

# array of the indices of all chemicals we are interested in
what_chemicals = np.array([0,1,2,3]) # h2, n2, nh3, ar

##################################################
# Functions

#returns Psat for a given compound (0 = hydrogen, 1 = nitrogen, 2= ammonia, 3= argon)
def P_sat(T,i):
    AAnt=np.array([3.54314, 3.7362 , 4.86886 , 4.46903])
    BAnt=np.array([99.395 , 264.651, 1113.928, 481.012])
    CAnt=np.array([ 7.726 ,  -6.788,  -10.409, 22.156 ])
    return 10 ** ( AAnt[i] - BAnt[i] / ( T + CAnt[i] ))

# TODO @Asbi: what does this funciton do? please explain for the public
def Ra_Rice(alpha, z, Ptot, T,i):
    K = P_sat(T, i) / Ptot
    return sum(z[i]*(K[i] - 1) / (1+alpha*(K[i]-1)))

#gives out the molar fractions of the liquid stream of the flash times the total molar flow
def flash_x(alpha, z, Ptot, T,i):
    K = P_sat(T, i) / Ptot
    return z[i] / (1+alpha*( K[i]-1) )

#gives out the molar fractions of the gas stream of the flash times the total molar flow
def flash_y(alpha, z, Ptot, T,i):
    K = P_sat(T, i)/Ptot
    return K[i] * z[i] / (1+alpha*( K[i]-1) )

#gives out the molar flows of the liquid stream of the flash
def flash_flow_x(alpha, z, Ptot, T, i):
    return (1-alpha) * flash_x(alpha, z, Ptot, T,i)

#gives out the molar flows of the gas stream of the flash
def flash_flow_y(alpha, z, Ptot, T, i):
    return alpha*flash_y(alpha, z, Ptot, T,i)


##################################################
# Flash Calculations

alpha_opt = scipy.optimize.brentq(Ra_Rice, 0, 1, args=(z, Ptot, T, what_chemicals))

print(alpha_opt)

# thank you alex :)
# bitte asbi :*

#print(flash_x(alpha_opt, z, Ptot, T,range(i)), flash_y(alpha_opt, z, Ptot, T, range(i) ))
#print(  flash_x(alpha_opt, z, Ptot, T,index), flash_y(alpha_opt, z, Ptot, T,index) )
