# -*- coding: utf-8 -*-
# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from scipy.integrate import odeint, RK45

plt.rc('text.latex', preamble=r'\usepackage{siunitx}')


from functions import * # Import the functions file

CONV = False
# Constants
n_steps_P = 5
n_steps_T = 100

#Temperature consticrions
T_reactor = 495 #[K]

T_min = 485
T_max = 510

#Pressure constrictions
P_reactor = 200 #[bar]

P_min = 190
P_max = 210 + 1

#Volume restricions
V_reactor = 30 # 15.36

V_min = V_reactor * 0.95
V_max = V_reactor * 1.05


c0 = np.array([4.47048999e+02, 1.49111618e+02, 6.27389419e+01, 1.87885526e-03]) #np.array([1861, 621, 0, 0])

print(c0[0] / (c0[1]*3))

T = np.linspace(T_min, T_max, 100)
n_steps_T = len(T)

P = np.arange(P_min, P_max, 5)
n_steps_P = len(P)
#V = np.linspace(V_min, V_max, n_steps_P)

conversion_H2 = np.zeros((n_steps_P, n_steps_T))
yield_NH3 = np.zeros((n_steps_P, n_steps_T))


#to find the nearest in the array
def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]










def reactor(c,x,T,P,V, i, j): #i and j are changed


    f_tot = sum(c)
    c_h2 = c[0]
    c_n2 = c[1]
    c_nh3= c[2]
    c_ar = c[3]


    k1 = 1.0e4*np.exp(-9.1e4/(r*T[i]))
    k2 = 1.3e10*np.exp(-1.41e5/(r*T[i]))

    dc_h2=1000*3*(-k1*(P[j]*c[0]/f_tot)**(0.5)*(P[j]*c[1]/f_tot)**(1.5)+k2*(P[j]*c[2]/f_tot));
    dc_n2=1000*((-k1*(P[j]*c[0]/f_tot)**(0.5)*(P[j]*c[1]/f_tot)**(1.5)+k2*(P[j]*c[2]/f_tot)));
    dc_nh3=1000*2*((+k1*(P[j]*c[0]/f_tot)**(0.5)*(P[j]*c[1]/f_tot)**(1.5)-k2*(P[j]*c[2]/f_tot)));
    dc_ar = 0

    return [dc_h2, dc_n2, dc_nh3, dc_ar]

#print(conc[cain,cbin,ccin,dadx_in,dbdx_in,dcdx_in],l)
dx= np.linspace(0,V_reactor,1000) #dimensionless length of the film, 10'000 steps

for i in range(0, n_steps_T):
    for j in range(0, n_steps_P):

        print('iteration: ', np.array([i, j]))
        solution = odeint(reactor,c0,dx, args=(T,P,V_reactor,i,j))

        u_h2 = solution[:,0]
        u_n2 = solution[:,1]
        u_nh3 = solution[:,2]
        u_ar = solution[:,3]

        #print(u_h2[-1], u_n2[-1], u_nh3[-1] , u_ar[-1])

        conversion_H2[j, i] = (c0[0] - u_h2[-1]) / c0[0]
        #selectivity_NH3[i, j] = u_nh3[i,j] / sum(np.array([u_h2[i, j], u_n2[i, j], u_nh3[i, j], u_ar[i, j]])
        yield_NH3[j, i] = u_nh3[-1]* 3 / c0[0] / 2


#linestyles = ['-', '-.', '-', '--', ':']
#colors     = ['green', 'green', 'blue', 'green', 'green', 'blue', 'blue', 'blue']

for i in range(0,n_steps_P):
    label = r'$P = \SI{' + str(round(P[i],0)) + r'}{\bar}$'
    if (CONV):
        plt.plot(T, conversion_H2[i, :], label=label, linewidth=1)
        plt.scatter(T[np.where(conversion_H2[i,:] == np.amax(conversion_H2[i,:]))], [np.amax(conversion_H2[i,:])], c='black')
        plt.ylabel(r'Conversion X$_\mathrm{H\textsubscript{2}}$ / -', fontsize=16)
    else:
        plt.plot(T, yield_NH3[i, :], label=label, linewidth=1)
        plt.scatter(T[np.where(yield_NH3[i,:] == np.amax(yield_NH3[i,:]))], [np.amax(yield_NH3[i,:])], c='black')
        plt.ylabel(r'Yield $\Phi$ / -', fontsize=16)


plt.xlabel(r'$T$ / K', fontsize=16)


if (CONV):
    plt.scatter(495, conversion_H2[2, np.where(T == find_nearest(T, 495))] , label='conditions used', c='black', marker='*')
else:
    plt.scatter(495, yield_NH3[2, np.where(T == find_nearest(T, 495))] , label='conditions used', c='black', marker='*')
plt.legend()
plt.tight_layout()


if(CONV):
    print('== Creating CONVERSION plot ==')
    plt.savefig('plots/conversion_T.pdf')
else:
    print('== Creating YIELD plot ==')
    plt.savefig('plots/yield_T.pdf')
