import numpy as np

#Inlet Temps
feed_T = np.array([298.0, 298.0, 298.0, 298.0]) # [K], H2 N2 NH3 Ar

#inlet molar flows
#old #feed_F = np.array([437.0, 146.0, 0.0, 0.0001]) # [mol/s], H2 N2 NH3 Ar
feed_F = np.array([431.1, 146.0, 0.0, 0.0001]) * 0.6550246379263867 # [mol/s], H2 N2 NH3 Ar
feed_H2 = np.array([431.1, 0, 0.0, 0.000]) * 0.6550246379263867 # [mol/s], H2 N2 NH3 Ar
feed_N2 = np.array([0, 146.0, 0.0, 0.0001]) * 0.6550246379263867 # [mol/s], H2 N2 NH3 Ar

#inlet Pressures
feed_P = np.array([3e6, 7e5, 7e5, 7e5]) # [Pa], H2 N2 NH3 Ar
feed_F_normalized = feed_F / sum(feed_F)

#######################################################
index = np.array([0,1,2,3])

molar_mass = np.array([2.015e-3, 28.013e-3, 17.031e-3, 39.948]) # [kg mol-1]


#######################################################
# Pressure after compressor 1
P_compressor = 2e7 # [Pa]

# Isentropic efficiency
eta = 0.75


# universal gas constant
R = 8.31446261815324 # universal gas constant [J/mol/K]


######################################################
# Reactor Temperature
T_reactor = 495 # [K]

#Reactor Pressure
P_reactor = 200 # [bar]

#Volume of the reactor
V_reactor = 30 # [m3]


#####################################################

#thermal conductivity Steel 316 https://www.azom.com/properties.aspx?ArticleID=863
lambda_steel = 15.5 # [W/K/m]

#Steel thickness
steel_thickness = 0.025 # [m] (one inch)

#Thermal conductivity of Steel
h_steel = lambda_steel / steel_thickness

#Cooling water Temperatures
cooling_water_inlet_T = 283.15 # [K]
cooling_water_outlet_T = 287.15 # [K]


#Heating steam Temperatures:
steam_inlet_T = 700 # [K]

steam_outlet_T = 500 # [K]


####################################################
#Flash Temperature
T_flash = 303 # [K]

#Flash Pressure
P_flash = 50 # [bar]


#index (to include all substances)
i = 4


####################################################
# purge percentage
purge = 0.0001

####################################################

# atm in pa
atm = 101325
