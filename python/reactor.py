# -*- coding: utf-8 -*-
# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from scipy.integrate import odeint, RK45

plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

from functions import * # Import the functions file
from constants import *

##################################################
# Constants and initial conditions

c0 = np.array([6.88896308e+04, 2.29400639e+04, 2.62370790e+04, 2.93734618e-025])

T = 400

V = 2000
p = 200

atm = 101325

k1 = 1.0e4  * np.exp(-9.1e4/(r*T))
k2 = 1.3e10 * np.exp(-1.41e5/(r*T))

##################################################
# Functions

def reactor(c,x,T,p,V):
    f_tot = sum(c)
    c_h2 = c[0]
    c_n2 = c[1]
    c_nh3= c[2]

    k1 = 1.0e4*np.exp(-9.1e4/(r*T))
    k2 = 1.3e10*np.exp(-1.41e5/(r*T))

    dc_h2=1000*3*(-k1*(p*c[0]/f_tot)**(0.5)*(p*c[1]/f_tot)**(1.5)+k2*(p*c[2]/f_tot));
    dc_n2=1000*((-k1*(p*c[0]/f_tot)**(0.5)*(p*c[1]/f_tot)**(1.5)+k2*(p*c[2]/f_tot)));
    dc_nh3=1000*2*((+k1*(p*c[0]/f_tot)**(0.5)*(p*c[1]/f_tot)**(1.5)-k2*(p*c[2]/f_tot)));

    return [dc_h2, dc_n2, dc_nh3]

##################################################
# Mass balance for reactor


dx= np.linspace(0,V,100000) #dimensionless length of the film, 10'000 steps

solution = odeint(reactor,c0,dx, args=(T,p,V))

#print('Conversion of H2: ' + str(1-(solution[-1][0]/c0[0]))) #conversion of hydrogen

plt.plot(dx,solution[:,0], linestyle='-', color='black', label=r'$c_\textrm{H\textsubscript{2}}$', linewidth=1)
plt.plot(dx,solution[:,1], linestyle=':', color='black', label=r'$c_\textrm{N\textsubscript{2}}$', linewidth=1)
plt.plot(dx,solution[:,2], linestyle='-.', color='black', label=r'$c_\textrm{NH\textsubscript{3}}$', linewidth=1)

plt.xlabel(r'$V$ / \si{\cubic\meter}', fontsize=16)
plt.ylabel(r'$c$ / \si{\mol\per\cubic\meter}', fontsize=16)

plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)
plt.tight_layout()
plt.legend()
plt.savefig("reactor.pdf")
