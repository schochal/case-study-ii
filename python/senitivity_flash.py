# -*- coding: utf-8 -*-
# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from scipy.integrate import odeint, RK45

from functions import * # Import the functions file

ISOLATED = False

##################################################
# Constants In

# composition in the beginning
z = np.array([1.68032885e+02, 5.61062471e+01, 2.48749684e+02, 1.87885526e-03])

# Constants
n_steps_P = 5
n_steps_T = 100

#Temperature consticrions
# temperature
T = 303 # [K]

T_min = 275
T_max = 325

#Pressure constrictions
Ptot = 50 #[bar]

P_min = 40
P_max = 100 + 1

#Volume restricions
V_reactor = 30 # 15.36

V_min = V_reactor * 0.95
V_max = V_reactor * 1.05


T = np.linspace(T_min, T_max, 100)
n_steps_T = len(T)

P = np.arange(P_min, P_max, 10)
n_steps_P = len(P)
#V = np.linspace(V_min, V_max, n_steps_P)

isolated_NH3 = np.zeros((n_steps_T, n_steps_P))
purity_NH3 = np.zeros((n_steps_T, n_steps_P))


#to find the nearest in the array
def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]


# total pressure
Ptot = 150 # [bar]


# array of the indices of all chemicals we are interested in
what_chemicals = np.array([0,1,2,3]) # h2, n2, nh3, ar

##################################################
# Functions

#returns Psat for a given compound (0 = hydrogen, 1 = nitrogen, 2= ammonia, 3= argon)
def P_sat(T,i):
    AAnt=np.array([3.54314, 3.7362 , 4.86886 , 4.46903])
    BAnt=np.array([99.395 , 264.651, 1113.928, 481.012])
    CAnt=np.array([ 7.726 ,  -6.788,  -10.409, 22.156 ])
    return 10 ** ( AAnt[i] - BAnt[i] / ( T + CAnt[i] ))

# TODO @Asbi: what does this funciton do? please explain for the public
def Ra_Rice(alpha, z, Ptot, T,i, a, b):
    K = P_sat(T[a], i) / Ptot[b]
    return sum(z[i]*(K[i] - 1) / (1+alpha*(K[i]-1)))

#gives out the molar fractions of the liquid stream of the flash times the total molar flow
def flash_x(alpha, z, Ptot, T,i, a, b):
    K = P_sat(T[a], i) / Ptot[b]
    return z[i] / (1+alpha*( K[i]-1) )

#gives out the molar fractions of the gas stream of the flash times the total molar flow
def flash_y(alpha, z, Ptot, T,i, a, b):
    K = P_sat(T[a], i)/Ptot[b]
    return K[i] * z[i] / (1+alpha*( K[i]-1) )

#gives out the molar flows of the liquid stream of the flash
def flash_flow_x(alpha, z, Ptot, T, i, a, b):
    return (1-alpha) * flash_x(alpha, z, Ptot, T,i, a, b)

#gives out the molar flows of the gas stream of the flash
def flash_flow_y(alpha, z, Ptot, T, i, a, b):
    return alpha*flash_y(alpha, z, Ptot, T,i, a, b)


##################################################
# Flash Calculations
for a in range(0, n_steps_T):
    for b in range(0, n_steps_P):
            alpha_opt = scipy.optimize.brentq(Ra_Rice, 0, 1, args=(z, P, T, what_chemicals, a, b))
            print(alpha_opt)

            isolated_NH3[a,b] = flash_flow_x(alpha_opt, z, P, T,what_chemicals, a, b)[2] / z[2]
            #print('fractions isolated: ', isolated_NH3[a,b])
            purity_NH3[a,b] = flash_x(alpha_opt, z, P, T,what_chemicals, a, b)[2] / sum(flash_x(alpha_opt, z, P, T,what_chemicals, a, b))
            #print('purity test: ', purity_NH3[a,b])


#linestyles = ['-', '-.', ':', '--', '-', '-.', ':']
#colors     = ['green', 'green', 'green', 'green', 'blue', 'blue', 'blue', 'blue']

for i in range(0,n_steps_P):
    label = '$P = \SI{' + str(round(P[i],0)) + r'}{\bar}$'
    #plt.plot(T, isolated_NH3[:,i], label=label, linestyle=linestyles[i], color=colors[i], linewidth=1)
    if (ISOLATED):
        plt.plot(T, isolated_NH3[:,i], label=label, linewidth=1)
        plt.ylabel(r'$\dot{n}_\mathrm{NH\textsubscript{3},l} / \dot{n}_\mathrm{NH\textsubscript{3},in}$', fontsize=16)
        #plt.scatter(T[np.where(isolated_NH3[:,i] == np.amax(isolated_NH3[:,i]))], [np.amax(isolated_NH3[:, i])], c='black')
    else:
        plt.plot(T, purity_NH3[:,i], label=label, linewidth=1)
        plt.ylabel(r'Molar fraction $x_\mathrm{NH\textsubscript{3},l}$ / -', fontsize=16)

plt.xlabel(r'$T$ / K', fontsize=16)


# thank you alex :)
# bitte asbi :*
#print('where =', find_nearest(T, 303))
if (ISOLATED):
    plt.scatter(303, isolated_NH3[np.where(T == find_nearest(T, 303)), 1] , c='black', marker='*', label='conditions used')
else:
    plt.scatter(303, purity_NH3[np.where(T == find_nearest(T, 303)), 1] , c='black', marker='*', label='conditions used')
plt.legend()
plt.tight_layout()
if (ISOLATED): 
    print('== creating ISOLATED plot ==')
    plt.savefig('plots/flash_purity_T_isolated.pdf')
else:
    print('== creating NORMAL plot ==')
    plt.savefig('plots/flash_purity_T.pdf')
