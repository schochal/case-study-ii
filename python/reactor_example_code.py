def reactor(c,x,T,p,V):
    f_tot = sum(c)
    c_h2 = c[0]
    c_n2 = c[1]
    c_nh3= c[2]
    c_ar = c[3]
    k1 = 1.0e4*np.exp(-9.1e4/(r*T))
    k2 = 1.3e10*np.exp(-1.41e5/(r*T))
    dc_h2=1000*3*(-k1*(p*c[0]/f_tot)**(0.5)*(p*c[1]/f_tot)**(1.5)+k2*(p*c[2]/f_tot));
    dc_n2=1000*((-k1*(p*c[0]/f_tot)**(0.5)*(p*c[1]/f_tot)**(1.5)+k2*(p*c[2]/f_tot)));
    dc_nh3=1000*2*((+k1*(p*c[0]/f_tot)**(0.5)*(p*c[1]/f_tot)**(1.5)-k2*(p*c[2]/f_tot)));
    dc_ar = 0
    return [dc_h2, dc_n2, dc_nh3, dc_ar]

dx= np.linspace(0,V,100000) #dimensionless length of the film, 10'000 steps
solution = odeint(reactor,c0,dx, args=(T,p,V))
output = solution[-1,:]
