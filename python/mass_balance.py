#-*- coding: utf-8 -*-
# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
import scipy.optimize
from scipy.optimize import curve_fit
from scipy.stats import t
from scipy.integrate import odeint

from constants import *
from functions import *

##################################################
# Constants 

L = 10 # [m]
Q = 10 # [m**3/s]


