# Case Study II

Repository for our code of Case Study II. For all non-coders: you can just click on a file and download it like that without having to log into [gitlab](https://gitlab.ethz.ch)

## Questions Day 1

| **Question** | **Answer** |
| ---: | :--- |
| Should we use the same conditions as the ones we found in Case Study 1? | kind of |
| Can we use the same Mass balance as in Case Study 1? | not really |
| Do we assume ideal mixing behaviour (no change in Volume)? | yes |
| Can we assume adiabatic behaviour for the mixer? | yes |
| Are the two pressures at the start the same? | depends on where we get it, but genrally yes |
| how low can the temperature go with the temperature? | find a value with which we are happy (500K - 700K) |
| what should the pressure be at the mixer 2 (10bar/100bar)? | answered? |
| Do we need to define the reactor in detail? | no, just an ideal PFR |
| What are the key process parameters? | ? | 
| Could you please explain us the unit operation models? | Which simplifications do we use? What equations do we thus use? |

## Questions Day 2

| **Question** | **Answer** |
| ---: | :--- |
| Is it okay to use the Shomate Equation on Nist, even though it is not dependent on the temperature? | Yes, because the equations we use already account for that. |

## Questions Day 3

| **Question** | **Answer** |
| ---: | :--- |
| Can we use the ideal gas law for the pressure calculation? | |
